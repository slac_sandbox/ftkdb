#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <bitset>         
#include <vector>      
#include <math.h>

/* #include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ftkcommon/core.h"
#include "ftkvme/VMEManager.h"
#include "aux/aux_vme_regs.h"
#include "aux/aux.h"
*/
#define STANDALONE
namespace daq 
{
  namespace ftk 
  {


    signed long long aux_asr(signed long long input , int shift, int width) {

      // deal with the sign separately
      unsigned long long shifted = abs(input);

      // make the baseline shift
      if (shift > 0) shifted = (shifted << shift);
      if (shift < 0) {
        // clear bits that will "go negative"
        shifted &= ~((static_cast<long long>(1) << -shift) - 1);
        shifted = (shifted >> -shift);
      }

      // save bits within the width (subtracting one bit for sign)
      signed long long output = shifted & ((static_cast<long long>(1) << (width-1))-1);

      bool overflow = false;
      if (static_cast<unsigned long long>(llabs(output)) != shifted) overflow = true;

      // reinstate the sign.
      if (input < 0) output *= -1;

      if (overflow)  output = (1 << (width-1)) - 1;

      return output;

    }



    bool aux_tfconst_load(u_int slot, u_int fpga, const std::string& gcon, int SECTOR, bool checksum, bool dump) {

      std::vector<u_int> words;

      const int NSECTORS = 4096;
      const int NCOORDS = 11;
      const int NCHI = 6;
      const int NPIX = 3;
      const int NSCT = 5;

      const int NBITS_EXPO  = 5;
      const int NBITS_FIXED = 18;
      const int NBITS_FLOAT = 13;
      const int KERN_SHIFT = 12;
      const int KAVE_SHIFT = 4;


      // dummy variables to read this into.
      std::string stra; int sector; 
      double kaverage[NCHI];
      double kernel[NCHI][NCOORDS];
      double majkk[NCOORDS][NCOORDS];
      std::vector<double> majinvkk;

      // variables for the check sum
      std::bitset<1540> bm_cs;
      for (int i = 0; i < 16; i++) bm_cs.set(i);
      unsigned sum1 = 0, sum2 = 0, curr = 0;
      unsigned currval = 0;

      // read in the file.
      std::ifstream input (gcon.c_str());
      while (input >> stra) {
	std::cout << stra << std::endl;

        // look for the "sector" keyword
        if (stra != "sectors") continue;

        words.clear();
        majinvkk.clear();

        input >> sector;
        if (SECTOR >= 0 && SECTOR != sector) continue;

        // sector += 2048;
        if (sector >= NSECTORS) break;

        // if it _is_ right, look for kaverages and kernel
        // load these into memory.
        while (input >> stra) if (stra == "kaverages") break;
        for (int i = 0; i < NCHI; i++) {
          input >> kaverage[i]; 
        }
        while (input >> stra) if (stra == "kernel")    break;
        for (int i = 0; i < NCHI; i++) {
          for (int j = 0; j < NCOORDS; j++) {
            input >> kernel[i][j];
          }
        }

        // calculate the majority recovery constants
        for (int i = 0; i < NCOORDS; i++) {
          for (int j = 0; j < NCOORDS; j++) {
            majkk[i][j] = 0.;

            // we actually just need the block diagonal bit,
            // but make the whole matrix, regardless.
            for (int r = 0; r < NCHI; r++) 
              majkk[i][j] += kernel[r][i] * kernel[r][j];
          }
        }


        // calculate the matrix inverses.
        for (int pi = 0; pi < 2*NPIX; pi += 2) {

          //get determinant.
          double det = majkk[pi][pi] * majkk[pi+1][pi+1] - majkk[pi+1][pi] * majkk[pi][pi+1];

          // negatives to help with t-vector calculation
          majinvkk.push_back( - majkk[pi+1][pi+1]/det);
          majinvkk.push_back(   majkk[pi+1][pi  ]/det);
          majinvkk.push_back( - majkk[pi  ][pi  ]/det);
        }
        for (int si = NPIX*2; si < NPIX*2 + NSCT; si++) {
          majinvkk.push_back( - 1./majkk[si][si]);
        }


        ///////
        // At this point we have all the constants ready.
        // Now we just need to arrange the bits for the TF.
        // Put these into bitsets of the appropriate size,
        // and then copy them into one gigantic set of 'bits'
        // (1540 = 77 * 20 long) as the full set of constants
        // for that sector. 

        int currbit = 0;        std::bitset<1540> bits;

        for (int i = 0; i < NCHI; i++) {
          for (int j = 0; j < NCOORDS; j++) {
            std::bitset<18> kerbit(daq::ftk::aux_asr(round(kernel[i][j] * pow(2., KERN_SHIFT)), 0, NBITS_FIXED));
            for (int bi = 0; bi < NBITS_FIXED; bi++, currbit++) bits.set(currbit, kerbit[bi]);
          }
          std::bitset<18> kavbit(daq::ftk::aux_asr(round(kaverage[i] * pow(2., KAVE_SHIFT)), 0, NBITS_FIXED));
          for (int bi = 0; bi < NBITS_FIXED; bi++, currbit++) bits.set(currbit, kavbit[bi]);
        }


        for (auto v : majinvkk) {

          int expo   = NBITS_FLOAT - ceil(log2(fabs(v))) - 1;
          int signif = daq::ftk::aux_asr(round(v * pow(2, expo)), 0, NBITS_FLOAT);

          std::bitset<17> conbit((signif << 5) | expo);
          for (int bi = 0; bi < NBITS_EXPO+NBITS_FLOAT-1; bi++, currbit++) bits.set(currbit, conbit[bi]);
        }


        // just a mask to save 20 bits at a time.
        // this is the size of the constants.
        std::bitset<1540> bm;
        for (int i = 0; i < 20; i++) bm.set(i);

        // "rev up" the system.
        words.clear();
        unsigned long long tmp_word = ((bits & bm).to_ullong() << 20) | (sector << 1) | 1;
        for (int i = 0; i < 3; i++) {
          words.push_back(tmp_word & 0xffffffff);
          words.push_back((tmp_word >> 32) & 0xffffffff);
        }

        for (int i = 0; i < 77; i++) { // forwards

          // shift the bits past the 20-bit mask, saving one word.
          unsigned long long data = ((bits >> (i*20)) & bm).to_ullong();

          // (20 bits of data ) + (7 bits of addr) + (12 bits of sector) + ("write")
          unsigned long long word = (data << 20) | (i << 13) | (sector << 1) | 1;

          // split the 64 bit word into two pieces for VME
          words.push_back(word & 0xffffffff);
          words.push_back((word >> 32) & 0xffffffff);

        }

        // turn off writing.
        words.push_back(sector << 1); 
        words.push_back(0);

        if (dump) {
          FILE *fp=stdout;
          char outfile [50];
          sprintf (outfile, "tf_hex_const/sec%04d", sector);
	  std::cout << "writing " << sector << " to: " << outfile << std::endl;
          if ((fp=fopen(outfile, "w")) == NULL) {
            fprintf(stderr,"Cannot open file: %s.\n", outfile);
            exit(1);
          } else {
            for (auto w : words) fprintf(fp, "%08x\n", w);
            fclose(fp);
          }
        }


        if (checksum) { // checksum

          for (int x = 0; x < 97; x++) {

            curr = ((bits >> (x*16)) & bm_cs).to_ullong();
            sum2 = ((curr + sum1 + sum2) & 0xffff) % 65535;
            sum1 = ((curr + sum1) & 0xffff) % 65535;

          }

          if (SECTOR >= 0) {

            unsigned stemp1 = sum1;
            unsigned stemp2 = sum2;
            for (int sx = sector + 1; sx < NSECTORS; sx++) {
              for (int x = 0; x < 97; x++) {

                curr = 0;
                stemp2 = ((curr + stemp1 + stemp2) & 0xffff) % 65535;
                stemp1 = ((curr + stemp1) & 0xffff) % 65535;

              }
            }
            currval = ((stemp2 << 16) + stemp1);

          } else currval = ((sum2 << 16) + sum1);
        }


	/*        if (!dump) {
          VMEInterface *vme=VMEManager::global().aux(slot,fpga);
          vme->write_word(TMODE,0x1);
          vme->write_word(0x70,0x1);

          vme->write_block(0x12 << 16, words);

          vme->write_word(0x70, 0x0);
          vme->write_word(0x70, 0x2);      
          vme->write_word(TMODE,0x0);
        }
	*/
        if (SECTOR >= 0) break;
      }
      
      input.close(); // close the file -- done with loading


      if (checksum) std::cout << "checksum: " << std::hex << currval << std::endl;

      // let's see what we've got!!!
      // for (auto w : words) std::cout << std::setfill('0') << std::setw(8) << std::hex << w << std::endl;

      return false;

    }

    
  } //namespcae daq
  } //namespcae ftk


#ifdef STANDALONE
/////////////////////////////////////////////////////////////////////////////
// Stand alone application
//   - Compiled when -DSTANDALONE
/////////////////////////////////////////////////////////////////////////////

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>


/** Parsing input parmaeters and calling the am_init function
 *
 */
int main(int argc, char **argv) 
{ 
 
  using namespace  boost::program_options ;
  
  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("15"), "The card slot.")
    ("fpga", value< std::string >()->default_value("1"), "The FPGA ID.")
    ("tfconstpath", value< std::string >(), "Location of input constants.")
    ("sector", value< std::string >()->default_value("-1"), "Load a sector.")
    ("checksum", bool_switch()->default_value(false),  "Run and print the checksum.")
    ("hex_dump", bool_switch()->default_value(false),  "Dump the checksum to files.")
    ;
  
  positional_options_description p;
  p.add("tfconstpath", 1);

  variables_map vm;
  try 
    {
      store(command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl; 
      return 1;
    }
  
  notify(vm);
  
  if( vm.count("help") ) // if help is required, then desc is printed to output
    {	
      std::cout << std::endl <<  desc << std::endl ; 
      return 0;	
    }
  
  u_int slot = std::stoi( vm["slot"].as<std::string>() );
  u_int fpga = std::stoi( vm["fpga"].as<std::string>() );
  int sector = std::stoi( vm["sector"].as<std::string>() );
  std::string tfconstpath  = vm["tfconstpath"].as<std::string>();

  bool checksum = vm["checksum"].as<bool>();
  bool dump = vm["hex_dump"].as<bool>();
  std::cout << "dump  " << dump << std::endl;
  return  daq::ftk::aux_tfconst_load(slot, fpga, tfconstpath, sector, checksum, dump) ;
}
#endif
